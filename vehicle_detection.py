from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import os.path

import re
import sys
import tarfile
from six.moves import urllib
import shutil

import json

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


from detection import vehicle_detection
from detection import load_image_into_numpy_array
from classification import vehicle_classification
# 清空文件夹
shutil.rmtree('./data/image_crop/')
os.mkdir('./data/image_crop/')
shutil.rmtree('./data/image_detection/')
os.mkdir('./data/image_detection/')
shutil.rmtree('./data/image_result/')
os.mkdir('./data/image_result/')

if tf.__version__ < '1.10.0':
    raise ImportError('Please upgrade your tensorflow installation to v1.10.* or later!')

NUM_CLASSES1 = 1
NUM_CLASSES2 = 20
num_top_predictions = 5

#需要检测图片的路径
test_img_path = os.path.join('./data/test.jpg')
#汽车检测模型训练生成的文件
PATH_TO_CKPT1 = os.path.join('./data/frozen_inference_graph1.pb')
#汽车检测的label文件
PATH_TO_LABELS1 = os.path.join('./data/labels_items1.txt')
#汽车检测模型训练生成的文件
PATH_TO_CKPT2 = os.path.join('./data/frozen_inference_graph2.pb')
#汽车检测的label文件
PATH_TO_LABELS2 = os.path.join('./data/labels_items2.txt')
#分类模型训练的文件
model_file = os.path.join('./data/my_inception_v4_freeze.pb')
#lable文件
label_file = os.path.join('./data/my_inception_v4_freeze.label')
#微软雅黑字体
myfont = FontProperties(fname=os.path.join('./data/msyh.ttf'))
ttfont = ImageFont.truetype('./data/msyh.ttf',15)
#png图片格式识别
if test_img_path[-4:] == '.png':
    img_path = Image.open(test_img_path)
    img_path = img_path.convert("RGB")
    img_path.save('./data/temporary.jpg')
    test_img_path = os.path.join('./data/temporary.jpg')
# 结果图片处理
def processImage(path,content):
    image1 = Image.open(path)
    lw, bh = image1.size 
    image_result = Image.new("RGB",(lw, bh+20),color=(255,255,255))
    lw, lh = image_result.size
    image_1 = image1.resize((lw, bh),)
    draw = ImageDraw.Draw(image_result)
    draw.text((5, 0),content,fill=(0,0,0),font = ttfont)
    image_result.paste(image1, (0, 20))
    image_result.save('./data/image_result/result{}.jpg'.format(j))

if __name__ == '__main__': 
    #第一次汽车检测
    result_detection = vehicle_detection(test_img_path, NUM_CLASSES1, PATH_TO_CKPT1, PATH_TO_LABELS1, './data/image_detection/image_detection.jpg')
    scores = result_detection[0]
    classes = result_detection[1]
    boxes = result_detection[2]
    image = Image.open(test_img_path)
    image_n = load_image_into_numpy_array(image)
    # 得分超过0.55的汽车的个数
    i = 0
    for sc in scores:
        for _ in sc:
            if _ > 0.55:
                i += 1
                
    # 若没有超过0.6的分数则输出
    if i == 0:
        #print('未识别出车辆！')
        data=[{'预测结果':'未识别出车辆！'}]
        jsonStr = json.dumps(data,ensure_ascii=False)
        print(jsonStr)
        result_detection = vehicle_detection(img_dir, NUM_CLASSES2, PATH_TO_CKPT2, PATH_TO_LABELS2, './data/image_detection/image_detection.jpg')
    else:
        for j in range(i):
            # 裁减图片
            xmin = boxes[0][j][0]
            xmax = boxes[0][j][1]
            ymin = boxes[0][j][2]
            ymax = boxes[0][j][3]
            img = Image.fromarray(image_n,'RGB')
            img = img.crop([img.size[0]*xmax*0.95,img.size[1]*xmin*0.95,img.size[0]*ymax*1.01,img.size[1]*ymin*1.01])
            # 保存裁减的图片
            img_dir = './data/image_crop/'+'crop'+'{}'.format(j)+'.jpg'
            img.save(img_dir)
            img_path = os.path.join(img_dir)
        
            # 第二次车辆检测
            result_detection = vehicle_detection(img_dir, NUM_CLASSES2, PATH_TO_CKPT2, PATH_TO_LABELS2, './data/image_detection/image_detection.jpg')
            scores = result_detection[0]
            classes = result_detection[1]
            # 显示汽车检测的路径    
            vehicle_detection(test_img_path, NUM_CLASSES2, PATH_TO_CKPT2, PATH_TO_LABELS2, './data/image_detection/image_detection.jpg')
            if 7 in classes[0][0:3]:
               # print('第{}辆汽车结果预测：'.format(j+1))
                data=[{'正在检测':'第{}辆汽车预测结果'.format(j+1)}]
                jsonStr = json.dumps(data,ensure_ascii=False)
                print(jsonStr)

                #型号分类
                detection_graph = tf.Graph()
                result_classification = vehicle_classification(img_path, model_file, label_file, num_top_predictions)
                predictions = result_classification[0]
                node_lookup = result_classification[1]
                top_k = result_classification[2]
                
                top_names = []
                Score = []
                id = 0
                for node_id in top_k:
                    human_string = node_lookup.id_to_string(node_id)
                    top_names.append(human_string)
                    score = predictions[node_id]
                    Score += [score]
                    if score > 0.1:
                        s = '%.2f' % (score*100)
                        data=[{'score':'{}%'.format(s), 'name':'{}'.format(human_string)}]
                        jsonStr = json.dumps(data,ensure_ascii=False)
                        print(jsonStr)
                        id += 1
                    else:
                        # print('库中可能没有该车型！')
                        if id == 0:
                            data=[{'error':'库中可能没有该车型！'}]
                            jsonStr = json.dumps(data,ensure_ascii=False)
                            print(jsonStr)
                            break
                            # print('id:[%d] name:[%s] (score = %.5f)' % (node_id, human_string, score))
                # 车辆分类的最高分数大于0.2，则显示汽车的型号分类
                if Score[0] > 0.2:
                    processImage(img_dir,'{}'.format(top_names[0]))
                # 车辆分类的最高得分低于0.2，则输出
                if Score[0] < 0.2:
                    processImage(img_dir,'库中可能没有该车型！')
                    data=[{'预测结果':'请换辆车试试！'}]
                    jsonStr = json.dumps(data,ensure_ascii=False)
                    print(jsonStr)
            else:
                data=[{'预测结果':'未识别出车辆！'}]
                jsonStr = json.dumps(data,ensure_ascii=False)
                print(jsonStr)
                break
# 删除生成的临时文件（可执行可不执行，做下一次检测的时候会替换掉）
#    os.remove('./data/temporary.jpg')
